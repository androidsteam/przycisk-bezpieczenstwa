package com.potop66.przyciskbezpieczenstwa;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    SharedPreferences.Editor edit;
    SharedPreferences sp;
    ImageButton btn;
    Context context=this;
    GPStracker tracker;
    final int REPEAT_TIME=1000*10;
    Boolean canClickBtn=true;
    String[] name_png = new String[]{
            "yellow.png",        //false
            "red-button.png"    //true
    };

    /** if users click this btn, then he/she will go to a map
     *
     * @param view
     */
    public void  maps (View view){
        startActivity(new Intent(context, Maps.class));
    }
    /**
     * if users click this, then he/she will call/call off alarm
     *
     * @param view
     */
    public void btn (View view) {
        if (canClickBtn) {
            canClickBtn=false;
            if (sp.getBoolean("clicked", false)) {
                // user call off alarm

                new CallOffAlarm().execute();
            } else {
                //user call alarm
                Location location = tracker.getLocation();
                new SendLocation().execute(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));

            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn=(ImageButton) findViewById(R.id.imageButton);
        sp = getSharedPreferences("btn_state", MODE_PRIVATE);
        tracker=new GPStracker(context);

        edit = sp.edit();
     if(! tracker.canGetLocation()){
         //alert dialog 
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
         builder.setCancelable(false);
         builder.setTitle(R.string.alert_title);
         builder.setMessage(R.string.alert_message);
         builder.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialogInterface, int i) {
                 //Brak zezwolenia na korzystanie z aplkiacji bez gps
                 finish();
             }
         });
         builder.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialogInterface, int i) {
                 // ustawienia
                 startActivity(new Intent(Settings.ACTION_SETTINGS));
                 dialogInterface.cancel();
             }
         });
         AlertDialog alert=builder.create();
         alert.show();
        }


        if(!sp.getBoolean("clicked", false)){
            try {
                btn.setImageDrawable(Drawable.createFromStream(context.getAssets().open(name_png[0]), Bitmap.CompressFormat.PNG.toString()));
            } catch (IOException e) {
                e.printStackTrace();
            }        }else {
            try {
                btn.setImageDrawable(Drawable.createFromStream(context.getAssets().open(name_png[1]), Bitmap.CompressFormat.PNG.toString()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);
        PendingIntent pending = PendingIntent.getBroadcast(context, 0, i,0);
        Calendar cal = Calendar.getInstance();
        // Start 60 seconds after boot completed
        cal.add(Calendar.SECOND, 10);
        //
        // Fetch every 60 seconds
        // InexactRepeating allows Android to optimize the energy consumption
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                cal.getTimeInMillis(), REPEAT_TIME, pending);
        Log.d("alarm", "alarm ustawiony");
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(! tracker.canGetLocation()){
            //alert dialog
            AlertDialog.Builder builder=new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle(R.string.alert_title);
            builder.setMessage(R.string.alert_message);
            builder.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //Brak zezwolenia na korzystanie z aplkiacji bez gps
                    finish();
                }
            });
            builder.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // ustawienia
                    startActivity(new Intent(Settings.ACTION_SETTINGS));
                }
            });
            AlertDialog alert=builder.create();
            alert.show();
        }
    }


//================================================================================================================================
//================================================================================================================================

    public class SendLocation extends AsyncTask<String, Integer, Integer> {
        String lon,lat;
        Boolean done=false;

        @Override
        protected Integer doInBackground(String... params) {
            APIHelper apiHelper=new APIHelper(context);
            lat=params[0];
            lon=params[1];
           String id= apiHelper.add_alarm(lat,lon);
            if(!id.equals("")) {
                edit.putInt("id", Integer.parseInt(id));
                edit.apply();
                done = true;
            }


            return null;
        }
        @Override
        protected void onPostExecute(Integer i) {
            super.onPostExecute(i);
            if(done) {
                Toast.makeText(context, "Alarm wywolany", Toast.LENGTH_LONG).show();
                try {
                    btn.setImageDrawable(Drawable.createFromStream(context.getAssets().open(name_png[1]), Bitmap.CompressFormat.PNG.toString()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                edit.putBoolean("clicked", true);
                edit.apply();

            }else{
                Toast.makeText(context, "ERROR", Toast.LENGTH_LONG).show();
            }
            canClickBtn=true;
        }
    }
//================================================================================================================================
//================================================================================================================================

    public class CallOffAlarm extends AsyncTask<String, Integer, Integer> {
            String result;
        @Override
        protected Integer doInBackground(String... params) {
            APIHelper apiHelper=new APIHelper(context);
            result=apiHelper.remove_alarm(sp.getInt("id",0));
            return null;
        }
        @Override
        protected void onPostExecute(Integer i) {
            super.onPostExecute(i);
            if(result.equals("ALARM_REMOVED")) {
                Toast.makeText(context, "Alarm odwolany", Toast.LENGTH_LONG).show();
                try {
                    btn.setImageDrawable(Drawable.createFromStream(context.getAssets().open(name_png[0]), Bitmap.CompressFormat.PNG.toString()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                edit.putBoolean("clicked", false);
                edit.apply();


            }else{
                Toast.makeText(context, "Sprobuj jeszcze raz", Toast.LENGTH_LONG).show();
            }
            canClickBtn=true;
        }
    }
}
