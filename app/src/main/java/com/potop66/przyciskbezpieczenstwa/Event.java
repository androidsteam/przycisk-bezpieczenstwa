package com.potop66.przyciskbezpieczenstwa;

/**
 * Class Event - Structure for parsing Events
 */

public class Event {


    private Double eventLat = 0.0;
    private Double eventLng = 0.0;

    public Event( Double event_lat, Double event_lng){
        // Event constructor

        this.eventLat = event_lat;
        this.eventLng = event_lng;
    }

    public Double getEventLng() {
        return eventLng;
    }

    public void setEventLng(Double eventLng) {
        this.eventLng = eventLng;
    }

    public Double getEventLat() {
        return eventLat;
    }

    public void setEventLat(Double eventLat) {
        this.eventLat = eventLat;
    }

}
