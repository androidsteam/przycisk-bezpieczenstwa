package com.potop66.przyciskbezpieczenstwa;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Activity showing map on events
 */

public class Maps extends AppCompatActivity {

    private String PREFS_NAME = "Inspirit";

    Context context = this;
    public GoogleMap googleMap;
    public ArrayList<Event> events;
    SharedPreferences sp;
    Integer id;
    String authkey;
    Integer ary_size;
    JSONObject jObject;
    Integer clicked_id;
    LatLng position;
    MarkerOptions markerOptions;
    Location location;

    public void back (View w ){
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        GPStracker gpStracker = new GPStracker(context);
         location = gpStracker.getLocation();


        // Screen Orientation
        SharedPreferences sharedpreferences = this.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int screen_orientation = sharedpreferences.getInt("screen_orientation", 0);
        switch(screen_orientation){
            case 0:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case 1:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            default:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        // Database & Map Initialization
        try {
            initializeMap();
            changeCamera(location);
        } catch(Exception e){
            e.printStackTrace();
        }

    }

    /**     that function add marker to a map
     *
     * @param i
     */
    public void addMarker(LatLng i){
        markerOptions = new MarkerOptions().position(i);
        googleMap.addMarker(markerOptions);
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){
            @Override
            public boolean onMarkerClick(Marker arg0){
                position = arg0.getPosition();
                return true;
                // Apply position to given event (TODO)
            }
        });
    }

    private void show_events(){
        sp = getSharedPreferences("Inspirit", MODE_PRIVATE);
        id = sp.getInt("id", 0);
        authkey = sp.getString("authkey", "");
        try {
            APIHelper apihelper = new APIHelper(context);
            JSONArray jsonArray;
                jsonArray = new JSONArray(apihelper.get_near_alarms(location.getLatitude()+"",location.getLongitude()+""));

            ary_size = jsonArray.length();
            Log.d("Inspirit", "Ary size = " + ary_size);
            events = new ArrayList<Event>();
            for (int i = 0; i < ary_size; i++) {
                try {
                    Log.d("Inspirit", "Adding event");
                    // Error below
                    jObject = jsonArray.getJSONObject(i);
                    events.add(new Event(
                            jObject.getDouble("lat"),
                            jObject.getDouble("lng")
                    ));
                    // Error up
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeMap(){
        if (googleMap == null){
            googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();

            // Ustaw typ mapy (z ustawień)
           SharedPreferences sharedpreferences = this.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
           int map_type = sharedpreferences.getInt("map_type",1);
            switch(map_type){
                case 0:
                    // Normalna mapa
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    break;
                case 1:
                    // Satelita (z napisami)
                    googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    break;
                case 2:
                    // Satelita (bez napisów)
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    break;
                case 3:
                    // Teren
                    googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                    break;
                default:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }

            googleMap.setMyLocationEnabled(true);
        }
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        new AddMapMarkers().execute();
    }

    @Override
    protected void onResume(){
        super.onResume();
        initializeMap();
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    public void changeCamera(Location location){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(),location.getLongitude()))
                .zoom(20)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }



    public class AddMapMarkers extends AsyncTask<String, Integer, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            events = new ArrayList<Event>();
            show_events();
            return 0;
        }

        @Override
        protected void onPostExecute(Integer i2) {
            // Dodanie markerów w pętli
            for(Event e: events){
                Log.d("Inspirit", "Adding marker");
                addMarker(new LatLng(e.getEventLat(), e.getEventLng()));
            }
        }
    }

}

