package com.potop66.przyciskbezpieczenstwa;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by potop66 on 24.01.16.
 */
public class Alarm extends BroadcastReceiver  {
    Context context;
    SharedPreferences sp;
    SharedPreferences.Editor edit;
    @Override
    public void onReceive(Context context, Intent intent){
    this.context=context;
        sp=context.getSharedPreferences("ID", Context.MODE_PRIVATE);
        edit=sp.edit();
        try {
            GPStracker gpStracker = new GPStracker(context);
            Location location = gpStracker.getLocation();
            new CheckingAlarms().execute(location.getLatitude() + "", location.getLongitude() + "");
        }catch(Exception e ){
            Log.d("ERROR KURWA",e+"");
        }

    }

    public void notification(Context context){
        long [] a=new long[]{500,500,500,500,500};
        NotificationCompat.Builder builder=new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.common_signin_btn_icon_dark);
        builder.setContentTitle("w poblizu zostal uruchoomiony alarm");
        builder.setAutoCancel(true);
        builder.setVibrate(a);
        Intent e=new Intent(context,Maps.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(Maps.class);
        stackBuilder.addNextIntent(e);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, builder.build());
    }


    public class CheckingAlarms extends AsyncTask <String, Integer, Integer>{
            Boolean alarms=false;
        @Override
        protected Integer doInBackground(String... strings) {
            APIHelper apiHelper=new APIHelper(context);
            String result_id=apiHelper.get_last_alarm_id();
            if(!result_id.equals("")){
                int id=Integer.parseInt(result_id);
                if(sp.getInt("id",0)<id){
                    //checking alarms
                    alarms=apiHelper.check_near_alarms_after_id(strings[0],strings[1],sp.getInt("id",0));
                }
                edit.putInt("id",id);
                edit.apply();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Integer i2) {
           if(alarms){
               notification(context);
           }
        }
    }
}



//pobiera id ) sprtawdaz czu jest wioeksze od poprzednio porbanego
//(jezeli tak) wysyla lokalizacje i sprawdza czy jest cos w promieniu 5 km
//( jezeli nie ) nie robi nic