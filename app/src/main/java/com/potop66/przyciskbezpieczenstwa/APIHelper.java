package com.potop66.przyciskbezpieczenstwa;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class containing functions to communicate with external API
 */

public class APIHelper {


    /**
     * Class context
     */
    protected Context context;
    /**
     * Path to server
     */
    protected String SERVER_PATH = "http://inspirit.ugu.pl/api2.php";
    /**
     * Variable containing boolean value about active http connection
     */
    static Boolean httpConnect = true;
    /**
     * Variable containing key to logcat tool
     */
    private String LOG_KEY = "Inspirit";
    /**
     * Variable containing name of SharedPreferences
     */
    private String PREFS_NAME = "btn_state";

    /**
     * Create class instance with given context
     *
     * @param context Context
     */
    public APIHelper(Context context) {
        this.context = context;
    }

    public String split_result(String result){
        try {
            String resultargs[] = result.split("\\^api\\^");
            result = resultargs[1];
            resultargs= result.split("\\^api\\^");
            result = resultargs[0];
        } catch (ArrayIndexOutOfBoundsException e){
            Log.d(LOG_KEY, "Split result = " + result);
            return result;
        }
        Log.d(LOG_KEY, "Split result = "+result);
        return result;

    }


    /**
     * Check that user has active internet connection
     * Sprawdź czy użytkownik ma połączenie internetowe
     *
     * @return	True if user have active internet connection
     * False if user doesn't have active internet connection
     * Prawda jeśli użytkownik ma połączenie sieciowe
     * Fałsz jeśli użytkownik nie ma połączenia sieciowego
     */

    public  boolean hasActiveInternetConnection() {
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.pl").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return httpConnect=(urlc.getResponseCode() == 200);
        } catch (IOException e) {
            httpConnect=false;
        }
        return false;
    }
    
     /**
     * Get alarms count
     */

    public String alarms_count(){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
            InputStream is = null;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(SERVER_PATH);
                // Tablica z wartościami dla POST'a
                List<NameValuePair> params = new ArrayList<NameValuePair>(1);
                params.add(new BasicNameValuePair("act", "5"));
                httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                // Odpowiedź serwera
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error in http connection " + e.toString());
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                result = split_result(result);
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error converting result " + e.toString());
            }
            switch (result) {
                case "NO_RESULTS":
                    return "NO_RESULTS";
                default:
                    return result;
            }
        } else {
            editor.apply();
            return "";
        }
    }

    /**
     * Get last alarm id
     */

    public String get_last_alarm_id(){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
            InputStream is = null;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(SERVER_PATH);
                // Tablica z wartościami dla POST'a
                List<NameValuePair> params = new ArrayList<NameValuePair>(1);
                params.add(new BasicNameValuePair("act", "6"));
                httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                // Odpowiedź serwera
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error in http connection " + e.toString());
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                result = split_result(result);
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error converting result " + e.toString());
            }
            switch (result) {
                case "":
                    return "NO_RESULTS";
                default:
                    return result;
            }
        } else {
            editor.apply();
            return "";
        }
    }


    /**
     * Get all alarms
     */

    public String get_all_alarms(){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
                InputStream is = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(SERVER_PATH);
                    // Tablica z wartościami dla POST'a
                    List<NameValuePair> params = new ArrayList<NameValuePair>(1);
                    params.add(new BasicNameValuePair("act", "1"));
                    httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                    // Odpowiedź serwera
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error in http connection " + e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    result = split_result(result);
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error converting result " + e.toString());
                }
                switch (result) {
                    case "NO_RESULTS":
                        return "NO_RESULTS";
                    default:

                        return result;
                }
        } else {
            editor.apply();
            return "";
        }
    }

    /**
     * Get near alarms
     * @param user_lat User latitude
     * @param user_lng User longitude
     */

    public String get_near_alarms(String user_lat, String user_lng){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
                InputStream is = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(SERVER_PATH);
                    // Tablica z wartościami dla POST'a
                    List<NameValuePair> params = new ArrayList<NameValuePair>(3);
                    params.add(new BasicNameValuePair("act", "3"));
                    params.add(new BasicNameValuePair("user_lat", user_lat+""));
                    params.add(new BasicNameValuePair("user_lng", user_lng+""));
                    httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                    // Odpowiedź serwera
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error in http connection " + e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    result = split_result(result);
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error converting result " + e.toString());
                }
                switch (result) {
                    case "NO_RESULTS":
                        return "NO_RESULTS";
                    default:
                        return result;
                }
        } else {
            editor.apply();
            return "";
        }
    }
    
    /**
     * Get near alarms after specified ID
     * @param user_lat User latitude
     * @param user_lng User longitude
     * @param alarm_id Alarm ID
     */

    public String get_near_alarms_after_id(String user_lat, String user_lng, Integer alarm_id){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
            InputStream is = null;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(SERVER_PATH);
                // Tablica z wartościami dla POST'a
                List<NameValuePair> params = new ArrayList<NameValuePair>(4);
                params.add(new BasicNameValuePair("act", "4"));
                params.add(new BasicNameValuePair("user_lat", user_lat+""));
                params.add(new BasicNameValuePair("user_lng", user_lng+""));
                params.add(new BasicNameValuePair("alarm_id", alarm_id+""));
                httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                // Odpowiedź serwera
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error in http connection " + e.toString());
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                result = split_result(result);
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error converting result " + e.toString());
            }
            switch (result) {
                case "NO_RESULTS":
                    return "NO_RESULTS";
                default:
                    return result;
            }
        } else {
            editor.apply();
            return "";
        }
    }
    
     /**
     * Check if there's any alarm with range of 5 km from user
     * @param user_lat User latitude
     * @param user_lng User longitude
     * @param alarm_id Alarm ID
     */

    public Boolean check_near_alarms_after_id(String user_lat, String user_lng, Integer alarm_id){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
            InputStream is = null;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(SERVER_PATH);
                // Tablica z wartościami dla POST'a
                List<NameValuePair> params = new ArrayList<NameValuePair>(4);
                params.add(new BasicNameValuePair("act", "7"));
                params.add(new BasicNameValuePair("user_lat", user_lat+""));
                params.add(new BasicNameValuePair("user_lng", user_lng+""));
                params.add(new BasicNameValuePair("alarm_id", alarm_id+""));
                httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                // Odpowiedź serwera
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error in http connection " + e.toString());
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                result = split_result(result);
            } catch (Exception e) {
                Log.e(LOG_KEY, "Error converting result " + e.toString());
            }
            switch (result) {
                case "1":
                    return true;
                case "0":
					return false;
                default:
                    return false;
            }
        } else {
            editor.apply();
            return false;
        }
    }

    /** Add alarm
     *
     * @param alarm_lat User latitude
     * @param alarm_lng User longitude
     * @return Response code
     */

    public String add_alarm(String alarm_lat, String alarm_lng){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
                InputStream is = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(SERVER_PATH);
                    // Tablica z wartościami dla POST'a
                    List<NameValuePair> params = new ArrayList<NameValuePair>(3);
                    params.add(new BasicNameValuePair("act", "0"));
                    params.add(new BasicNameValuePair("alarm_lat", alarm_lat+""));
                    params.add(new BasicNameValuePair("alarm_lng", alarm_lng+""));
                    httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                    // Odpowiedź serwera
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error in http connection " + e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    result = split_result(result);
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error converting result " + e.toString());
                }
                switch (result) {
                    case "ERROR":
                        return "";
                    default:
                        return result;
                }
        } else {
            return "";
        }
    }

    /**
     * Remove alarm
     * @param alarm_id ID of the button
     * @return Response code
     */

    public String remove_alarm(Integer alarm_id){

        // Check internet connection
        hasActiveInternetConnection();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("HttpConnect", httpConnect);
        editor.apply();
        String result = "";
        if (httpConnect) {
                InputStream is = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(SERVER_PATH);
                    // Tablica z wartościami dla POST'a
                    List<NameValuePair> params = new ArrayList<NameValuePair>(2);
                    params.add(new BasicNameValuePair("act", "2"));
                    params.add(new BasicNameValuePair("id", alarm_id+""));
                    httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                    // Odpowiedź serwera
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error in http connection " + e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    result = split_result(result);
                } catch (Exception e) {
                    Log.e(LOG_KEY, "Error converting result " + e.toString());
                }
                switch (result) {
                    case "ALARM_REMOVED":
                        return "ALARM_REMOVED";
                    case "INVALID_ALARM":
                        return "";
                    default:
                        return result;
                }
        } else {
            editor.apply();
            return "";
        }
    }

}
